#!/bin/env python3
LISTFILE="completeList.csv"

with open(LISTFILE) as f:
    for l in f.readlines():
        l = l.split("\n")[0]
        newpvname = l.split(",")[1]
        if len(newpvname.split(")")) > 1:
            prop = newpvname.split(")")[-1]
            prop = prop.replace(" ","")
            print("%s [%s]" % (newpvname, str(len(prop))))

#!/bin/env python3
LISTFILE="completeList.csv"
LISTPVS="pvs.list"
# Check the size of new property (limited to 20)
noerrors = 1

allpvs = set()

with open(LISTFILE) as f:
    props = set() # to check if there are duplicates
    linen = 0
    for l in f.readlines():
        l = l.split("\n")[0]
        newpvname = l.split(",")[1]
        if len(newpvname.split(")")) > 1:
            prop = newpvname.split(")")[-1]
            prop = prop.replace(" ","")
            if len(prop) > 20:
                print("Property invalid. File Name %s Line number %d Property %s" % (LISTFILE, linen, prop))
                noerrors = 0
            if prop in props:
                print("Duplicated property: ", prop)
            else:
                props.add(prop)

        linen+=1


with open(LISTPVS) as f:
    for l in f.readlines():
        l = l.split("\n")[0]
        prop = l.split(":")[-1]
        prop = prop.replace(" ","")
        if prop in allpvs:
            print("Duplicated property: ", prop)
        else:
            allpvs.add(prop)

        if prop not in props:
            print("This property is not in the list: ", prop)

print("Number of props ", len(props))
print("Number of PVs from ioc ", len(allpvs))

if noerrors:
    print("All properties are valid!")

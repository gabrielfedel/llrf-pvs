import sys

from epics import caget

LISTFILE="completeList.csv"
P="OCTOPUS-LLRF1::"
R=""
PD="OCTOPUS-010:"
RD="RFS-DIG-101:"
PR="OCTOPUS-010:"
RR="RFS-RFM-101:"

def caget_assert(PV,param_timeout=1, param_as_string=False):
    if param_as_string:
        status = caget(PV, as_string=True, timeout=1)
        assert status is not None and isinstance(status,str)
    else:
        status = caget(PV,timeout = param_timeout)
        assert status is not None
    return status


with open(LISTFILE) as f:
    for pv in f:
        pv = pv.replace(" ","").replace("\n", "").split(",")[-1]
        if pv == "" or pv == "DELETE":
            continue
        if len (pv.split("$(PD")) > 1: # Digitiser prefix
            pv = PD + RD + pv.split(")")[-1]
        elif len (pv.split("$(PR")) > 1: # RTM prefix
            pv = PR + RR + pv.split(")")[-1]
        elif len (pv.split("$(P")) > 1: # system prefix
            pv = P + R + pv.split(")")[-1]
        else:
            print("Invalid pv: ", pv)

        status = caget(pv, timeout=5)
        if status is None:
            print("PV not connected! ", pv)
        print(".", end="")
        sys.stdout.flush()

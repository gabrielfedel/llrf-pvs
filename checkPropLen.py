#!/bin/env python3
LISTFILE="completeList.csv"
# Check the size of new property (limited to 20)
noerrors = 1

with open(LISTFILE) as f:
    props = set() # to check if there are duplicates
    linen = 0
    for l in f.readlines():
        l = l.split("\n")[0]
        newpvname = l.split(",")[1]
        if len(newpvname.split(")")) > 1:
            prop = newpvname.split(")")[-1]
            prop = prop.replace(" ","")
            if len(prop) > 20:
                print("Property invalid. File Name %s Line number %d Property %s" % (LISTFILE, linen, prop))
                noerrors = 0
            if prop in props:
                print("Duplicated property: ", prop)
            else:
                props.add(prop)
        linen+=1

if noerrors:
    print("All properties are valid!")


in_file_name="/home/gabrielfedel/e3_gitlab/llrf/e3-llrfsystem/llrfsystem-loc/llrfsystemApp/Db/detune.template"
file_names=["detune.template.pvs"]

with open(in_file_name) as i, open(in_file_name + ".new", "w") as o:
    for line in i:
        for file_name in file_names:
            with open(file_name) as s:
                for rp in s:
                    rp_str = rp.replace(" ","").replace("\n", "").split(",")
                    if rp_str == ["", ""]:
                        continue
                    line = line.replace(*rp_str)
        o.write(line)

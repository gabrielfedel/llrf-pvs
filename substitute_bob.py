import glob

INFOLDER="/home/gabrielfedel/LLRF/gui-llrf/embedded/"

file_names = ["detune.template.pvs"]
in_file_name = INFOLDER + "detuning.bob"

print(in_file_name)
with open(in_file_name) as i, open(in_file_name + ".new", "w") as o:
    for line in i:
        for file_name in file_names:
            with open(file_name) as s:
                for rp in s:
                    rp_str = rp.replace(" ","").replace("\n", "").split(",")
                    if rp_str == ["", ""]:
                        continue
                    rp_str[0] = rp_str[0].split(")")[-1]
                    rp_str[1] = rp_str[1].split(")")[-1]
                    line = line.replace(*rp_str)
        o.write(line)



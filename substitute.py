import glob

INFOLDER="/home/gabrielfedel/e3_gitlab/llrf/e3-sis8300/m-epics-sis8300/sis8300App/Db/"

for in_file_name in glob.glob(INFOLDER + "*.template"): # get all templates
    print(in_file_name)
    with open(in_file_name) as i, open(in_file_name + ".new", "w") as o:
        for line in i:
            for file_name in glob.glob("./*.pvs"): # get all replacement files
                with open(file_name) as s:
                    for rp in s:
                        rp_str = rp.replace(" ","").replace("\n", "").split(",")
                        if rp_str == ["", ""]:
                            continue
                        print(rp_str)
                        line = line.replace(*rp_str)
            o.write(line)



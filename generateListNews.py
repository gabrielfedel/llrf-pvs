#!/bin/env python3
import glob

for file_name in glob.glob("./*.pvs"):
    with open(file_name) as f:
        for l in f.readlines():
            l = l.split("\n")[0]
            newpvname = l.split(",")[1]
            newpvname = newpvname.replace(" ", "")
            if (newpvname not in  ["DELETE", ""]):
                print(newpvname)
